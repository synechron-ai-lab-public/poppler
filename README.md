# Poppler python bindings

This fork makes poppler python bindings around `pdftotext` utility to provide
PDF conversion to text in python applications.

## Installation

Follow [INSTALL](./INSTALL) for instalation steps.
